-- MariaDB dump 10.19  Distrib 10.9.2-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: idp_moodle
-- ------------------------------------------------------
-- Server version	10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `simplesamlphp_kvstore`
--

DROP TABLE IF EXISTS `simplesamlphp_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simplesamlphp_kvstore` (
  `_type` varchar(30) NOT NULL,
  `_key` varchar(50) NOT NULL,
  `_value` longtext NOT NULL,
  `_expire` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`_key`,`_type`),
  KEY `SimpleSAMLphp_kvstore_expire` (`_expire`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simplesamlphp_kvstore`
--

LOCK TABLES `simplesamlphp_kvstore` WRITE;
/*!40000 ALTER TABLE `simplesamlphp_kvstore` DISABLE KEYS */;
INSERT INTO `simplesamlphp_kvstore` VALUES
('session','57224c414debdacc920037e098846516','O%3A18%3A%22SimpleSAML%5CSession%22%3A10%3A%7Bs%3A9%3A%22sessionId%22%3Bs%3A32%3A%2257224c414debdacc920037e098846516%22%3Bs%3A9%3A%22transient%22%3Bb%3A0%3Bs%3A7%3A%22trackid%22%3Bs%3A10%3A%2275e3c0acc3%22%3Bs%3A16%3A%22rememberMeExpire%22%3BN%3Bs%3A5%3A%22dirty%22%3Bb%3A0%3Bs%3A19%3A%22callback_registered%22%3Bb%3A0%3Bs%3A9%3A%22dataStore%22%3Ba%3A1%3A%7Bs%3A16%3A%22core%3Aerrorreport%22%3Ba%3A1%3A%7Bs%3A8%3A%2215af932c%22%3Ba%3A3%3A%7Bs%3A7%3A%22expires%22%3Bi%3A1704737240%3Bs%3A7%3A%22timeout%22%3Bi%3A14400%3Bs%3A4%3A%22data%22%3Ba%3A7%3A%7Bs%3A12%3A%22exceptionMsg%22%3Bs%3A33%3A%22SimpleSAML%5CError%5CNoState%3A%20NOSTATE%22%3Bs%3A14%3A%22exceptionTrace%22%3Bs%3A560%3A%22Backtrace%3A%0A6%20src%5CSimpleSAML%5CAuth%5CState.php%3A282%20%28SimpleSAML%5CAuth%5CState%3A%3AloadState%29%0A5%20modules%5Ccore%5Csrc%5CController%5CLogin.php%3A115%20%28SimpleSAML%5CModule%5Ccore%5CController%5CLogin%3A%3Aloginuserpass%29%0A4%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A163%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3AhandleRaw%29%0A3%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A75%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3Ahandle%29%0A2%20vendor%5Csymfony%5Chttp-kernel%5CKernel.php%3A202%20%28Symfony%5CComponent%5CHttpKernel%5CKernel%3A%3Ahandle%29%0A1%20src%5CSimpleSAML%5CModule.php%3A234%20%28SimpleSAML%5CModule%3A%3Aprocess%29%0A0%20public%5Cmodule.php%3A17%20%28N%2FA%29%22%3Bs%3A8%3A%22reportId%22%3Bs%3A8%3A%2215af932c%22%3Bs%3A7%3A%22trackId%22%3Bs%3A10%3A%2275e3c0acc3%22%3Bs%3A3%3A%22url%22%3Bs%3A70%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fcore%2Floginuserpass%22%3Bs%3A7%3A%22version%22%3Bs%3A5%3A%222.0.6%22%3Bs%3A7%3A%22referer%22%3Bs%3A7%3A%22unknown%22%3B%7D%7D%7D%7Ds%3A12%3A%22associations%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22authToken%22%3BN%3Bs%3A8%3A%22authData%22%3Ba%3A0%3A%7B%7D%7D','2024-01-08 22:07:20'),
('session','94e6ec9935844805d27c1705021ec6ff','O%3A18%3A%22SimpleSAML%5CSession%22%3A10%3A%7Bs%3A9%3A%22sessionId%22%3Bs%3A32%3A%2294e6ec9935844805d27c1705021ec6ff%22%3Bs%3A9%3A%22transient%22%3Bb%3A0%3Bs%3A7%3A%22trackid%22%3Bs%3A10%3A%226230f1a823%22%3Bs%3A16%3A%22rememberMeExpire%22%3BN%3Bs%3A5%3A%22dirty%22%3Bb%3A0%3Bs%3A19%3A%22callback_registered%22%3Bb%3A0%3Bs%3A9%3A%22dataStore%22%3Ba%3A1%3A%7Bs%3A22%3A%22%5CSimpleSAML%5CAuth%5CState%22%3Ba%3A1%3A%7Bs%3A43%3A%22_417e8575c80fa5fd8d4f1f5cecfd54cd6af677663c%22%3Ba%3A3%3A%7Bs%3A7%3A%22expires%22%3Bi%3A1704726292%3Bs%3A7%3A%22timeout%22%3Bi%3A3600%3Bs%3A4%3A%22data%22%3Bs%3A855%3A%22a%3A11%3A%7Bs%3A26%3A%22%5CSimpleSAML%5CAuth%5CSource.id%22%3Bs%3A5%3A%22admin%22%3Bs%3A30%3A%22%5CSimpleSAML%5CAuth%5CSource.Return%22%3Bs%3A58%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fadmin%2F%22%3Bs%3A32%3A%22%5CSimpleSAML%5CAuth%5CSource.ErrorURL%22%3BN%3Bs%3A21%3A%22LoginCompletedHandler%22%3Ba%3A2%3A%7Bi%3A0%3Bs%3A22%3A%22SimpleSAML%5CAuth%5CSource%22%3Bi%3A1%3Bs%3A14%3A%22loginCompleted%22%3B%7Ds%3A14%3A%22LogoutCallback%22%3Ba%3A2%3A%7Bi%3A0%3Bs%3A22%3A%22SimpleSAML%5CAuth%5CSource%22%3Bi%3A1%3Bs%3A14%3A%22logoutCallback%22%3B%7Ds%3A19%3A%22LogoutCallbackState%22%3Ba%3A1%3A%7Bs%3A36%3A%22%5CSimpleSAML%5CAuth%5CSource.logoutSource%22%3Bs%3A5%3A%22admin%22%3B%7Ds%3A33%3A%22%5CSimpleSAML%5CAuth%5CSource.ReturnURL%22%3Bs%3A58%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fadmin%2F%22%3Bs%3A48%3A%22%5CSimpleSAML%5CModule%5Ccore%5CAuth%5CUserPassBase.AuthId%22%3Bs%3A5%3A%22admin%22%3Bs%3A14%3A%22forcedUsername%22%3Bs%3A5%3A%22admin%22%3Bs%3A25%3A%22%5CSimpleSAML%5CAuth%5CState.id%22%3Bs%3A43%3A%22_417e8575c80fa5fd8d4f1f5cecfd54cd6af677663c%22%3Bs%3A28%3A%22%5CSimpleSAML%5CAuth%5CState.stage%22%3Bs%3A47%3A%22%5CSimpleSAML%5CModule%5Ccore%5CAuth%5CUserPassBase.state%22%3B%7D%22%3B%7D%7D%7Ds%3A12%3A%22associations%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22authToken%22%3BN%3Bs%3A8%3A%22authData%22%3Ba%3A0%3A%7B%7D%7D','2024-01-08 22:04:52'),
('session','9ac1b38e47df984cb9b2f5e75d0f4709','O%3A18%3A%22SimpleSAML%5CSession%22%3A10%3A%7Bs%3A9%3A%22sessionId%22%3Bs%3A32%3A%229ac1b38e47df984cb9b2f5e75d0f4709%22%3Bs%3A9%3A%22transient%22%3Bb%3A0%3Bs%3A7%3A%22trackid%22%3Bs%3A10%3A%22af6e4b247a%22%3Bs%3A16%3A%22rememberMeExpire%22%3BN%3Bs%3A5%3A%22dirty%22%3Bb%3A0%3Bs%3A19%3A%22callback_registered%22%3Bb%3A0%3Bs%3A9%3A%22dataStore%22%3Ba%3A1%3A%7Bs%3A16%3A%22core%3Aerrorreport%22%3Ba%3A1%3A%7Bs%3A8%3A%22e36c6567%22%3Ba%3A3%3A%7Bs%3A7%3A%22expires%22%3Bi%3A1704737092%3Bs%3A7%3A%22timeout%22%3Bi%3A14400%3Bs%3A4%3A%22data%22%3Ba%3A7%3A%7Bs%3A12%3A%22exceptionMsg%22%3Bs%3A33%3A%22SimpleSAML%5CError%5CNoState%3A%20NOSTATE%22%3Bs%3A14%3A%22exceptionTrace%22%3Bs%3A560%3A%22Backtrace%3A%0A6%20src%5CSimpleSAML%5CAuth%5CState.php%3A282%20%28SimpleSAML%5CAuth%5CState%3A%3AloadState%29%0A5%20modules%5Ccore%5Csrc%5CController%5CLogin.php%3A115%20%28SimpleSAML%5CModule%5Ccore%5CController%5CLogin%3A%3Aloginuserpass%29%0A4%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A163%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3AhandleRaw%29%0A3%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A75%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3Ahandle%29%0A2%20vendor%5Csymfony%5Chttp-kernel%5CKernel.php%3A202%20%28Symfony%5CComponent%5CHttpKernel%5CKernel%3A%3Ahandle%29%0A1%20src%5CSimpleSAML%5CModule.php%3A234%20%28SimpleSAML%5CModule%3A%3Aprocess%29%0A0%20public%5Cmodule.php%3A17%20%28N%2FA%29%22%3Bs%3A8%3A%22reportId%22%3Bs%3A8%3A%22e36c6567%22%3Bs%3A7%3A%22trackId%22%3Bs%3A10%3A%22af6e4b247a%22%3Bs%3A3%3A%22url%22%3Bs%3A70%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fcore%2Floginuserpass%22%3Bs%3A7%3A%22version%22%3Bs%3A5%3A%222.0.6%22%3Bs%3A7%3A%22referer%22%3Bs%3A7%3A%22unknown%22%3B%7D%7D%7D%7Ds%3A12%3A%22associations%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22authToken%22%3BN%3Bs%3A8%3A%22authData%22%3Ba%3A0%3A%7B%7D%7D','2024-01-08 22:04:52'),
('session','b53263e60d5824e5afaf39c860a12524','O%3A18%3A%22SimpleSAML%5CSession%22%3A10%3A%7Bs%3A9%3A%22sessionId%22%3Bs%3A32%3A%22b53263e60d5824e5afaf39c860a12524%22%3Bs%3A9%3A%22transient%22%3Bb%3A0%3Bs%3A7%3A%22trackid%22%3Bs%3A10%3A%224d7e55ce94%22%3Bs%3A16%3A%22rememberMeExpire%22%3BN%3Bs%3A5%3A%22dirty%22%3Bb%3A0%3Bs%3A19%3A%22callback_registered%22%3Bb%3A0%3Bs%3A9%3A%22dataStore%22%3Ba%3A1%3A%7Bs%3A16%3A%22core%3Aerrorreport%22%3Ba%3A1%3A%7Bs%3A8%3A%22bb781004%22%3Ba%3A3%3A%7Bs%3A7%3A%22expires%22%3Bi%3A1704737239%3Bs%3A7%3A%22timeout%22%3Bi%3A14400%3Bs%3A4%3A%22data%22%3Ba%3A7%3A%7Bs%3A12%3A%22exceptionMsg%22%3Bs%3A33%3A%22SimpleSAML%5CError%5CNoState%3A%20NOSTATE%22%3Bs%3A14%3A%22exceptionTrace%22%3Bs%3A560%3A%22Backtrace%3A%0A6%20src%5CSimpleSAML%5CAuth%5CState.php%3A282%20%28SimpleSAML%5CAuth%5CState%3A%3AloadState%29%0A5%20modules%5Ccore%5Csrc%5CController%5CLogin.php%3A115%20%28SimpleSAML%5CModule%5Ccore%5CController%5CLogin%3A%3Aloginuserpass%29%0A4%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A163%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3AhandleRaw%29%0A3%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A75%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3Ahandle%29%0A2%20vendor%5Csymfony%5Chttp-kernel%5CKernel.php%3A202%20%28Symfony%5CComponent%5CHttpKernel%5CKernel%3A%3Ahandle%29%0A1%20src%5CSimpleSAML%5CModule.php%3A234%20%28SimpleSAML%5CModule%3A%3Aprocess%29%0A0%20public%5Cmodule.php%3A17%20%28N%2FA%29%22%3Bs%3A8%3A%22reportId%22%3Bs%3A8%3A%22bb781004%22%3Bs%3A7%3A%22trackId%22%3Bs%3A10%3A%224d7e55ce94%22%3Bs%3A3%3A%22url%22%3Bs%3A70%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fcore%2Floginuserpass%22%3Bs%3A7%3A%22version%22%3Bs%3A5%3A%222.0.6%22%3Bs%3A7%3A%22referer%22%3Bs%3A7%3A%22unknown%22%3B%7D%7D%7D%7Ds%3A12%3A%22associations%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22authToken%22%3BN%3Bs%3A8%3A%22authData%22%3Ba%3A0%3A%7B%7D%7D','2024-01-08 22:07:19'),
('session','c6de0e8a3e463bc9cb88d38c08b0f9b2','O%3A18%3A%22SimpleSAML%5CSession%22%3A10%3A%7Bs%3A9%3A%22sessionId%22%3Bs%3A32%3A%22c6de0e8a3e463bc9cb88d38c08b0f9b2%22%3Bs%3A9%3A%22transient%22%3Bb%3A0%3Bs%3A7%3A%22trackid%22%3Bs%3A10%3A%2214e1b3f072%22%3Bs%3A16%3A%22rememberMeExpire%22%3BN%3Bs%3A5%3A%22dirty%22%3Bb%3A0%3Bs%3A19%3A%22callback_registered%22%3Bb%3A0%3Bs%3A9%3A%22dataStore%22%3Ba%3A1%3A%7Bs%3A16%3A%22core%3Aerrorreport%22%3Ba%3A1%3A%7Bs%3A8%3A%22aba1e096%22%3Ba%3A3%3A%7Bs%3A7%3A%22expires%22%3Bi%3A1704737088%3Bs%3A7%3A%22timeout%22%3Bi%3A14400%3Bs%3A4%3A%22data%22%3Ba%3A7%3A%7Bs%3A12%3A%22exceptionMsg%22%3Bs%3A33%3A%22SimpleSAML%5CError%5CNoState%3A%20NOSTATE%22%3Bs%3A14%3A%22exceptionTrace%22%3Bs%3A560%3A%22Backtrace%3A%0A6%20src%5CSimpleSAML%5CAuth%5CState.php%3A282%20%28SimpleSAML%5CAuth%5CState%3A%3AloadState%29%0A5%20modules%5Ccore%5Csrc%5CController%5CLogin.php%3A115%20%28SimpleSAML%5CModule%5Ccore%5CController%5CLogin%3A%3Aloginuserpass%29%0A4%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A163%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3AhandleRaw%29%0A3%20vendor%5Csymfony%5Chttp-kernel%5CHttpKernel.php%3A75%20%28Symfony%5CComponent%5CHttpKernel%5CHttpKernel%3A%3Ahandle%29%0A2%20vendor%5Csymfony%5Chttp-kernel%5CKernel.php%3A202%20%28Symfony%5CComponent%5CHttpKernel%5CKernel%3A%3Ahandle%29%0A1%20src%5CSimpleSAML%5CModule.php%3A234%20%28SimpleSAML%5CModule%3A%3Aprocess%29%0A0%20public%5Cmodule.php%3A17%20%28N%2FA%29%22%3Bs%3A8%3A%22reportId%22%3Bs%3A8%3A%22aba1e096%22%3Bs%3A7%3A%22trackId%22%3Bs%3A10%3A%2214e1b3f072%22%3Bs%3A3%3A%22url%22%3Bs%3A70%3A%22http%3A%2F%2Fidp.margarita.com%3A8080%2Fsimplesaml%2Fmodule.php%2Fcore%2Floginuserpass%22%3Bs%3A7%3A%22version%22%3Bs%3A5%3A%222.0.6%22%3Bs%3A7%3A%22referer%22%3Bs%3A7%3A%22unknown%22%3B%7D%7D%7D%7Ds%3A12%3A%22associations%22%3Ba%3A0%3A%7B%7Ds%3A9%3A%22authToken%22%3BN%3Bs%3A8%3A%22authData%22%3Ba%3A0%3A%7B%7D%7D','2024-01-08 22:04:48');
/*!40000 ALTER TABLE `simplesamlphp_kvstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simplesamlphp_tableversion`
--

DROP TABLE IF EXISTS `simplesamlphp_tableversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simplesamlphp_tableversion` (
  `_name` varchar(30) NOT NULL,
  `_version` int(11) NOT NULL,
  UNIQUE KEY `_name` (`_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simplesamlphp_tableversion`
--

LOCK TABLES `simplesamlphp_tableversion` WRITE;
/*!40000 ALTER TABLE `simplesamlphp_tableversion` DISABLE KEYS */;
INSERT INTO `simplesamlphp_tableversion` VALUES
('kvstore',2);
/*!40000 ALTER TABLE `simplesamlphp_tableversion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-01-24 12:19:44
