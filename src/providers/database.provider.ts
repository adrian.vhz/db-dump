import { DataSource } from "typeorm";
import { DataSourceConnection } from "src/db/data-source-connection";
import { Database } from "src/entities/database.entity";
import { ENTITY_DATABASE_REPOSITORY } from "src/utils/constants";

export const databaseProvider = {
	provide: ENTITY_DATABASE_REPOSITORY,
	useFactory: (appDataSource: DataSource) => appDataSource.getRepository(Database),
	inject: [DataSourceConnection]
}
