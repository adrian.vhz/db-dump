import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Repository {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	public token_type: string;
	
	@Column()
	public access_token: string;

	@Column()
	public refresh_token: string;

	@Column()
	public expires: string;

	@Column()
	public status: boolean;
}