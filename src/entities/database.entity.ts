import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Instance } from "./instance.entity";

@Entity()
export class Database {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	public engine: string;

	@Column()
	public status: boolean;
	
	@ManyToOne(() => Instance)
	public instance: Instance;
}
