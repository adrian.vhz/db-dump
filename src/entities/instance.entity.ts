import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Instance {
	@PrimaryGeneratedColumn()
	public id: number;

	@Column()
	public host: string;
	
	@Column()
	public port: string;
	
	@Column()
	public user: string;
	
	@Column()
	public password: string;
	
	@Column()
	public ssl: boolean;

	@Column()
	public rdms: string;

	@Column()
	public status: boolean;
}
