import { Injectable } from "@nestjs/common";
import { DatabaseRepository } from "./repositories/database.repository";
import { AppManager } from "./managers/app.manager";

@Injectable()
export class AppService {
    public constructor(
        private readonly databaseRepository: DatabaseRepository,
        private readonly appManager: AppManager
    ) { }

    public getHello(): string {
        return "Hello World!";
    }

    public async execBackup() {
        const databases = await this.databaseRepository.find({
            where: {
                status: true,
                rdms: {
                    status: true
                }
            },
            relations: {
                rdms: true
            }
        });

        for (const database of databases) {
            this.appManager.createDbBackup(database);
        }
    }
}
