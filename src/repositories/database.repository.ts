import { Inject, Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { Database } from "src/entities/database.entity";
import { ENTITY_DATABASE_REPOSITORY } from "src/utils/constants";

@Injectable()
export class DatabaseRepository extends Repository<Database> {
	public constructor(
		@Inject(ENTITY_DATABASE_REPOSITORY) databaseRepository: Repository<Database>
	) {
		super(databaseRepository.target, databaseRepository.manager, databaseRepository.queryRunner);
	}

	public async findAll() {
		return this.findAll();
	}
}
