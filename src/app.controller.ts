import { Controller, Get, Post } from "@nestjs/common";
import { AppService } from "./app.service";

@Controller()
export class AppController {
    public constructor(
        private readonly appService: AppService
    ) { }

    @Get()
    public getHello(): string {
        return this.appService.getHello();
    }

    @Get('exec-backup')
    public execBackup() {
        return this.appService.execBackup();
    }
}
