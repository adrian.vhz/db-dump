import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { DatabaseRepository } from "./repositories/database.repository";
import { DataSourceConnection } from "./db/data-source-connection";
import { databaseProvider } from "./providers/database.provider";
import { AppManager } from "./managers/app.manager";

@Module({
    imports: [],
    controllers: [AppController],
    providers: [AppService, AppManager, DatabaseRepository, DataSourceConnection, databaseProvider]
})
export class AppModule { }
