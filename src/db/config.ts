import { DataSourceOptions } from "typeorm";

export const config: DataSourceOptions = {
	type: 'mariadb',
	// host: process.env.DATABASE_HOST,
	// port: parseInt(process.env.DATABASE_PORT, 10),
	// username: process.env.DATABASE_USER,
	// password: process.env.DATABASE_PASSWORD,
	// database: process.env.DATABASE_NAME,
	host: "localhost",
	port: parseInt("3306", 10),
	username: "root",
	password: "",
	database: "db_backup_db",
	entities: [
		__dirname + '/../**/*.entity{.ts,.js}',
	],
	synchronize: true
}
