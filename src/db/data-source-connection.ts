import { DataSource } from "typeorm";
import { config } from "./config";

export class DataSourceConnection extends DataSource {
    public constructor() {
        super(config);
        this.init();
    }

	public init() {
		this.initialize();
	}
}
